# == Schema Information
#
# Table name: steps
#
#  id          :integer          not null, primary key
#  image       :string
#  description :string
#  seconds     :integer
#  order       :integer
#  recipe_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Step < ApplicationRecord
  belongs_to :recipe

  validates :image, presence: true, length: {maximum: 500}
  validates :description, presence: true, length: {maximum: 500}
  validates :seconds, presence: true, numericality: { greater_than_or_equal_to: 0, message: "can't be negative" }
  validates :order, presence: true, numericality: { greater_than: 0, message: "can't be negative" }

end
