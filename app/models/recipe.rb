# == Schema Information
#
# Table name: recipes
#
#  id    :integer          not null, primary key
#  name  :string
#  image :string
#

class Recipe < ApplicationRecord
  validates :name, presence: true, length: { maximum: 300 }
  validates :image, presence: true

  has_many :recipe_ingredients, dependent: :destroy
  has_many :ingredients, through: :recipe_ingredients
  has_many :steps, dependent: :destroy

  accepts_nested_attributes_for :recipe_ingredients

  def quantity(ingredient)
    if ingredients.include?(ingredient)
      return recipe_ingredients.where(ingredient_id: ingredient.id).first.quantity
    else
      return 0
    end
  end

  def energy
    energy = 0
    recipe_ingredients.each do |ri|
      energy += ri.ingredient.energy / 100 * ri.quantity
    end
    return energy
  end

  def proteins
    proteins = 0
    recipe_ingredients.each do |ri|
      proteins += ri.ingredient.proteins / 100 * ri.quantity
    end
    return proteins
  end

  def carbohydrates
    carbohydrates = 0
    recipe_ingredients.each do |ri|
      carbohydrates += ri.ingredient.carbohydrates / 100 * ri.quantity
    end
    return carbohydrates
  end

  def fat
    fat = 0
    recipe_ingredients.each do |ri|
      fat += ri.ingredient.fat / 100 * ri.quantity
    end
    return fat
  end

  def cook_time
    ct = 0
    steps.each do |step|
      ct += step.seconds
    end
    return ct
  end
end
