# == Schema Information
#
# Table name: ingredients
#
#  id            :integer          not null, primary key
#  name          :string
#  energy        :float
#  fat           :float
#  proteins      :float
#  carbohydrates :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Ingredient < ApplicationRecord
  validates :name, presence: true, length: { maximum: 200 }
  validates :energy, numericality: { greater_than_or_equal_to: 0, message: "Energy can't be negative" }

  has_many :recipe_ingredients, dependent: :destroy
  has_many :recipes, through: :recipe_ingredients
end
