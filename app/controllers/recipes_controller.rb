class RecipesController < ApplicationController

  before_action :set_recipe, only: [:show, :edit, :update, :destroy]

  def index
    @recipes = Recipe.all
    if @recipes.empty?
      add_flash_messages("error", ["There are no recipes yet"], false )
    end
  end

  def new
    if Ingredient.count == 0
      redirect_to new_ingredient_path
    else
      @recipe = Recipe.new
    end
  end

  def create
    @recipe = Recipe.new(recipe_params)
    ingredients_hash = ingredients_params
    unless ingredients_hash.blank?
      update_ingredients ingredients_params
      if @recipe.recipe_ingredients.empty?
        add_flash_messages("error", ["Ingredients can't be blank"], false)
        render :new
      elsif @recipe.save
        redirect_to new_recipe_step_path(@recipe.id)
      else
        add_flash_messages("error", @recipe.errors.full_messages, false)
        render :new
      end
    end
  end

  def show
  end

  def edit
  end

  def update
    Recipe.transaction do
      update_ingredients ingredients_params
      if @recipe.update(recipe_params)
        redirect_to recipes_path
      else
        add_flash_messages("error", @recipe.errors.full_messages, false)
        render :edit
      end
    end
  end

  def destroy
    @recipe.destroy
    redirect_to recipes_path
  end

  private

  def update_ingredients(ingredients_hash)
    ingredients_hash.each do |id, quantity|
      if quantity.to_f == 0
        @recipe.recipe_ingredients.where(ingredient_id: id).first.try(:destroy)
      elsif @recipe.ingredients.pluck(:id).include?(id)
        @recipe.recipe_ingredients.where(ingredient_id: id).first.update(quantity: quantity)
      else
        @recipe.recipe_ingredients.build(ingredient_id: id, quantity: quantity)
      end
    end
  end

  def set_recipe
    @recipe = Recipe.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    add_flash_messages("error", ["This recipe does not exist"], true)
    redirect_to recipes_path
  end

  def recipe_params
    params.require(:recipe).permit(:name, :image)
  end

  def ingredients_params
    params.require("ingredients").permit!
  rescue ActionController::ParameterMissing
    add_flash_messages("error", ["Ingredients can't be blank"], false)
    render :new
  end
end
