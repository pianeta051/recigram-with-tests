class StepsController < ApplicationController
  before_action :set_step, only: [:edit, :update, :destroy, :up, :down]
  before_action :set_recipe

  # GET /steps/new
  def new
    @step = Step.new
  end

  # POST /steps
  # POST /steps.json
  def create
    @step = @recipe.steps.new(step_params)
    @step.order = @recipe.steps.count + 1
    # @step = Step.new(step_params)
    # @step.recipe_id = @recipe.id
    if @step.save
      if params[:finish]
        add_flash_messages("success", ["Recipe successfully created"], true)
        redirect_to recipe_path(@recipe)
      else
        redirect_to new_recipe_step_path(@recipe)
      end
    else
      add_flash_messages("danger", @step.errors.full_messages, false)
      render :new
    end
  end

  # PATCH/PUT /steps/1
  # PATCH/PUT /steps/1.json
  def update
    if @step.update(step_params)
      add_flash_messages("success", ["Step was successfully updated"], true)
      redirect_to edit_recipe_path(@recipe)
    else
      add_flash_messages("error", @step.errors.full_messages, true)
      redirect_to edit_recipe_path(@recipe)
    end
  end

  # DELETE /steps/1
  # DELETE /steps/1.json
  def destroy
    Step.transaction do
      @recipe.steps.where("'order' > ?", @step.order).each do |step|
        step.update(order: step.order - 1)
      end

      @step.destroy
    end
    redirect_to edit_recipe_path(@recipe)
  end

  def up
    unless @step.order==1
      previous_step = @recipe.steps.where(order:@step.order-1)
      previous_step.update(order: @step.order)
      @step.update(order: @step.order-1)
    end
      redirect_to edit_recipe_path(@recipe)
  end

  def down
    unless @step.order==@recipe.steps.count
      next_step=@recipe.steps.where(order:@step.order+1)
      next_step.update(order: @step.order)
      @step.update(order: @step.order+1)
    end
      redirect_to edit_recipe_path(@recipe)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_step
      @step = Step.find(params[:id] || params[:step_id])
    rescue ActiveRecord::RecordNotFound
      add_flash_messages("error", ["This step does not exist"], true)
      redirect_to recipes_path
    end

    def set_recipe
      @recipe = Recipe.find(params[:recipe_id])
    rescue ActiveRecord::RecordNotFound
      add_flash_messages("error", ["This recipe does not exist"], true)
      redirect_to recipes_path
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def step_params
      params.require(:step).permit(:image, :description, :seconds)
    end
end
