class IngredientsController < ApplicationController
  before_action :set_ingredient, only: [:show, :edit, :update, :destroy]

  # GET /ingredients
  def index
    @ingredients = Ingredient.all
    if @ingredients.empty?
      add_flash_messages("error", ["There are no ingredients yet"], false )
    end
  end

  # GET /ingredients/1
  # GET /ingredients/1.json
  def show
  end

  # GET /ingredients/new
  def new
    @ingredient = Ingredient.new
  end

  # GET /ingredients/1/edit
  def edit
  end

  # POST /ingredients
  # POST /ingredients.json
  def create
    @ingredient = Ingredient.new(ingredient_params)
    if @ingredient.save
      add_flash_messages("success", ["Ingredient succesfully created"], true )
      redirect_to @ingredient
    else
      add_flash_messages("error", @ingredient.errors.full_messages, true)
      render :new
    end
  end

  # PATCH/PUT /ingredients/1
  # PATCH/PUT /ingredients/1.json
  def update
    if @ingredient.update(ingredient_params)
      add_flash_messages("success", ["Ingredient succesfully updated"], true )
      redirect_to @ingredient
    else
      add_flash_messages("error", @ingredient.errors.full_messages, true)
      render :edit
    end
  end

  # DELETE /ingredients/1
  def destroy
    @ingredient.destroy

    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ingredient
      @ingredient = Ingredient.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      add_flash_messages("error", ["This ingredient does not exist"], true)
      redirect_to ingredients_path
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ingredient_params
      params.require(:ingredient).permit(:name, :energy, :fat, :proteins, :carbohydrates)
    end
end
