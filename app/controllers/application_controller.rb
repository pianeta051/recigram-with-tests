class ApplicationController < ActionController::Base

  def add_flash_messages(type, messages, persist)
    if persist
      if flash[type].blank?
        flash[type] = messages
      else
        flash[type] += messages
      end
    else
      if flash.now[type].blank?
        flash.now[type] = messages
      else
        flash.now[type] += messages
      end
    end
  end
end
