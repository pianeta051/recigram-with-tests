Rails.application.routes.draw do

  resources :ingredients
  get 'pages/home'
  root 'pages#home'
  resources :recipes do
    resources :steps, except: [:index, :show, :edit] do
      put 'up', to: 'steps#up', as: :up
      put 'down', to: 'steps#down', as: :down
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
