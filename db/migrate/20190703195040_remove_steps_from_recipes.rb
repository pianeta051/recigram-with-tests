class RemoveStepsFromRecipes < ActiveRecord::Migration[5.2]
  def change
    remove_column :recipes, :steps, :string
  end
end
