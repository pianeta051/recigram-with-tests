class CreateSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :steps do |t|
      t.string :image
      t.string :description
      t.integer :seconds
      t.integer :order
      t.references :recipe, foreign_key: true

      t.timestamps
    end
  end
end
