Feature: Ingredients
I should be able to add steps to my recipes

Background:
  Given I have a recipe in my inventory

  Scenario: Listing steps
    Given The recipe has some steps
    When I visit the recipe page
    Then I should see a list of its steps

  Scenario: Adding a new step to the recipe
    When I edit the recipe
    And I add a new step to the recipe
    Then I should see the new step in the recipe details

  Scenario: Adding steps to new recipe
    When I create a new recipe
    And I add some steps to the new recipe
    Then I should see the steps in the recipe details

  Scenario: Editing a step
    Given The recipe has some steps
    When I edit the recipe
    And I change the description of a step
    Then I should see the new step description in the recipe details

  @javascript
  Scenario: Deleting a step
    Given The recipe has some steps
    When I edit the recipe
    And I delete a step
    Then I should not see the step in the recipe details
    And Order numbers should be updated

  Scenario: Changing steps order
    Given The recipe has some steps
    When I change the steps order
    Then I should see the new order in the recipe details
