Feature: Recipes
I should be able to see and upload recipes to the app

Scenario: Listing recipes
  Given There are several recipes
  When I visit the Recipes page
  Then I should see a list of recipes

Scenario: Adding a new recipe
  Given There are several ingredients uploaded
  When I submit a new recipe
  Then I should see the new recipe name in the list
  And It should have the ingredients I specified

Scenario: Changing the name of a recipe
  Given I have a recipe in my inventory
  When I change the name of a recipe
  Then I should see the edited name in the list and not the old one

Scenario: Changing the recipe ingredients
  Given I have a recipe in my inventory
  When I change the recipe ingredients
  Then I should see the new ingredients in its description

@javascript
Scenario: Deleting a recipe
  Given I have a recipe in my inventory
  When I delete the recipe
  Then I should not see the name in the list

Scenario: Reading a recipe
  Given There are several recipes
  When I visit the Recipes page
  And I select one recipe
  Then I should see the recipe details

Scenario: Checking nutritional info
  Given I have a recipe in my inventory
  Then I should see the nutritional info

Scenario: Checking cook time
  Given I have a recipe in my inventory
  And The recipe has some steps
  When I visit the recipe page
  Then I should see its cook time
