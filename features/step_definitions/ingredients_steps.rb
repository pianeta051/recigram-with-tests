Given("There are several ingredients uploaded") do
  @tomato = FactoryBot.create(:ingredient, name: "Tomato")
  @lentils = FactoryBot.create(:ingredient, name: "Lentils")
end

When("I visit the ingredients page") do
  visit root_path
  click_on "Ingredients"
end

Then("I should see a list of ingredients") do
  expect(page).to have_content(@tomato.name)
  expect(page).to have_content(@lentils.name)
end


When("I submit a new ingredient") do
  visit ingredients_path
  click_on "New ingredient"
  @lettuce = FactoryBot.build(:ingredient, name: "Lettuce")
  fill_in "ingredient_name", with: @lettuce.name
  fill_in "ingredient_energy", with: @lettuce.energy
  fill_in "ingredient_fat", with: @lettuce.fat
  fill_in "ingredient_proteins", with: @lettuce.proteins
  fill_in "ingredient_carbohydrates", with: @lettuce.carbohydrates
  click_on "Save"
end

Then("I should see the new ingredient in the list") do
  visit ingredients_path
  expect(page).to have_content(@lettuce.name)
end


Given("There is a ingredient uploaded") do
  @ingredient = FactoryBot.create(:ingredient)
end

When("I change the name of the ingredient") do
  visit ingredients_path
  page.find('li.list-group-item', text: @ingredient.name).click_on('Edit')
  fill_in "ingredient_name", with: "New name"
  click_on "Save"
end

Then("I should see the new ingredient name in the list") do
  visit ingredients_path
  expect(page).to have_content("New name")
  expect(page).not_to have_content(@ingredient.name)
end

When("I delete the ingredient") do
  visit ingredients_path
  page.find('li.list-group-item', text: @ingredient.name).click_on('Delete')
  page.driver.browser.switch_to.alert.accept
end

Then("I should not see the ingredient in the list") do
  expect(page).not_to have_content(@ingredient.name)
end

When("I select a ingredient") do
  click_on(@tomato.name)
end

Then("I should see the ingredient details") do
  expect(page).to have_content(@tomato.name)
  expect(page).to have_content(@tomato.energy)
  expect(page).to have_content(@tomato.fat)
  expect(page).to have_content(@tomato.proteins)
  expect(page).to have_content(@tomato.carbohydrates)
end
