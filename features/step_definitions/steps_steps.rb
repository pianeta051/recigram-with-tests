Given("The recipe has some steps") do
  3.times do |i|
    FactoryBot.create(:step, recipe: @recipe, order: i+1)
  end
end

When("I visit the recipe page") do
  visit recipe_path(@recipe)
end

Then("I should see a list of its steps") do
  @recipe.steps.each do |step|
    expect(page).to have_content(step.description)
  end
end

When("I edit the recipe") do
  visit edit_recipe_path(@recipe)
end

When("I add a new step to the recipe") do
  click_on "New step"

  @step = FactoryBot.build(:step)

  fill_in "step_image", with: @step.image
  fill_in "step_seconds", with: @step.seconds
  fill_in "step_description", with: @step.description

  click_on "Finish"
end

Then("I should see the new step in the recipe details") do
  visit recipe_path(@recipe)
  expect(page).to have_content(@step.description)
end

When("I create a new recipe") do
  recipe = FactoryBot.build(:recipe)
  visit new_recipe_path
  fill_in "recipe_name", with: recipe.name
  fill_in "recipe_image", with: recipe.image
  ingredient = Ingredient.first
  fill_in "#{ingredient.name}_quantity", with: 100

end

When("I add some steps to the new recipe") do

  click_on "Create steps"

  @step1 = FactoryBot.build(:step)
  @step2 = FactoryBot.build(:step)

  fill_in "step_image", with: @step1.image
  fill_in "step_seconds", with: @step1.seconds
  fill_in "step_description", with: @step1.description

  click_on "Next step"

  fill_in "step_image", with: @step2.image
  fill_in "step_seconds", with: @step2.seconds
  fill_in "step_description", with: @step2.description

  click_on "Finish"
end

Then("I should see the steps in the recipe details") do
  expect(page).to have_content(@step1.description)
  expect(page).to have_content(@step2.description)
end


When("I change the description of a step") do
  @changed_step = @recipe.steps.first
  @old_description = @changed_step.description
  @new_description = "My new description"
  fill_in "step_#{@changed_step.order}_description", with: @new_description
  page.find('div.list-group-item', text: @changed_step.order).click_on('Save')
end

Then("I should see the new step description in the recipe details") do
  visit recipe_path(@recipe)
  expect(page).to have_content(@new_description)
  expect(page).not_to have_content(@old_description)
end


When("I delete a step") do
  @deleted_step = @recipe.steps.first
  page.find('div.list-group-item', text: @deleted_step.description).click_on('Delete')
  page.driver.browser.switch_to.alert.accept
end

Then("I should not see the step in the recipe details") do
  visit recipe_path(@recipe)
  expect(page).not_to have_content(@deleted_step.description)
end

Then("Order numbers should be updated") do
  @recipe.steps.order(:order).each_with_index do |step, i|
    expect(page.find('div.list-group-item', text: step.description)).to have_content(i+1)
  end
end

When("I change the steps order") do
  visit edit_recipe_path(@recipe)

  @first_step = @recipe.steps.first
  @last_step = @recipe.steps.last

  page.find('div.list-group-item', text: @first_step.description).click_on('Down')
  page.find('div.list-group-item', text: @last_step.description).click_on('Up')
end

Then("I should see the new order in the recipe details") do
  visit recipe_path(@recipe)
  expect(page.find('div.list-group-item', text: @first_step.description)).to have_content(3)
  expect(page.find('div.list-group-item', text: @last_step.description)).to have_content(2)
end
