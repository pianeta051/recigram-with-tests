Given("There are several recipes") do
  @pizza = FactoryBot.create(:recipe_with_ingredients, name: 'Pizza')
  @spaguetti = FactoryBot.create(:recipe_with_ingredients, name: 'Spaguetti')
end

When("I visit the Recipes page") do
  visit root_path
  click_on 'Recipes'
end

Then("I should see a list of recipes") do
  expect(page).to have_content('Pizza')
  expect(page).to have_content('Spaguetti')
end

When("I submit a new recipe") do
  @sandwich = FactoryBot.build(:recipe, name: 'Sandwich')
  visit recipes_path
  click_button 'New recipe'
  fill_in 'recipe_name', with: @sandwich.name
  fill_in 'recipe_image', with: @sandwich.image

  fill_in "#{@tomato.name}_quantity", with: 100
  fill_in "#{@lentils.name}_quantity", with: 100

  click_button 'Create steps'
end

Then("I should see the new recipe name in the list") do
  visit recipes_path
  expect(page).to have_content(@sandwich.name)
end

Given("I have a recipe in my inventory") do
  @recipe = FactoryBot.create(:recipe_with_ingredients, ingredients_count: 2)
end

When("I change the name of a recipe") do
  visit recipes_path
  page.find('div.card', text: @recipe.name).click_button('Edit')
  fill_in "recipe_name", with: "Salad"
  click_button "Save"
end

Then ("I should see the edited name in the list and not the old one") do
  visit recipes_path
  expect(page).to have_content("Salad")
  expect(page).not_to have_content(@recipe.name)
end

When("I delete the recipe") do
  visit recipes_path
  page.find('div.card', text: @recipe.name).click_button('Delete')
  page.driver.browser.switch_to.alert.accept
end

Then("I should not see the name in the list") do
  visit recipes_path
  expect(page).not_to have_content(@recipe.name)
end

When("I select one recipe") do
  page.find('div.card', text: @pizza.name).click_button('Cook')
end

Then("I should see the recipe details") do
  expect(page).to have_content(@pizza.name)
  expect(page).to have_css("img[src*='#{@pizza.image}']")
  @pizza.ingredients.each do |ingredient|
    expect(page).to have_content(ingredient.name)
  end
  @pizza.steps.each do |step|
    expect(page).to have_content(step.description)
  end
end

Then("It should have the ingredients I specified") do
  visit recipes_path
  page.find('div.card', text: @sandwich.name).click_button('Cook')
  expect(page).to have_content(@tomato.name)
  expect(page).to have_content(@lentils.name)
end

When("I change the recipe ingredients") do
  @deleted_ingredient = @recipe.ingredients.first
  @changed_ingredient = @recipe.ingredients.last
  @new_ingredient = FactoryBot.create(:ingredient)

  visit edit_recipe_path(@recipe)

  fill_in "#{@deleted_ingredient.name}_quantity", with: 0
  fill_in "#{@changed_ingredient.name}_quantity", with: 3
  fill_in "#{@new_ingredient.name}_quantity", with: 100

  click_on "Save"
end

Then("I should see the new ingredients in its description") do
  visit recipe_path(@recipe)

  expect(page).not_to have_content(@deleted_ingredient.name)
  expect(page).to have_content("3.0g #{@changed_ingredient.name}")
  expect(page).to have_content(@new_ingredient.name)
end

Then("I should see the nutritional info") do
  visit recipe_path(@recipe)
  expect(page).to have_content(((@recipe.energy*100).round/100.0).to_s + " kcal")
  expect(page).to have_content(((@recipe.fat*100).round/100.0).to_s + " g")
  expect(page).to have_content(((@recipe.carbohydrates*100).round/100.0).to_s + " g")
  expect(page).to have_content(((@recipe.proteins*100).round/100.0).to_s + " g")
end

Then("I should see its cook time") do
  expect(page).to have_content(@recipe.cook_time.to_s + " seconds")
end
