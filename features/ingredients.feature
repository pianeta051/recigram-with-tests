Feature: Ingredients
I should be able to manage ingredients

Scenario: Listing ingredients
  Given There are several ingredients uploaded
  When I visit the ingredients page
  Then I should see a list of ingredients

Scenario: Adding a new ingredient
  When I submit a new ingredient
  Then I should see the new ingredient in the list

Scenario: Changing the name of a ingredient
  Given There is a ingredient uploaded
  When I change the name of the ingredient
  Then I should see the new ingredient name in the list

@javascript
Scenario: Removing a ingredient
  Given There is a ingredient uploaded
  When I delete the ingredient
  Then I should not see the ingredient in the list

Scenario: Reading a ingredient
  Given There are several ingredients uploaded
  When I visit the ingredients page
  And I select a ingredient
  Then I should see the ingredient details
