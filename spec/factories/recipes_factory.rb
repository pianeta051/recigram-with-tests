FactoryBot.define do
  factory :recipe do
    name { Faker::Food.dish }
    image { Faker::LoremPixel.image("1000x1000", false, 'food') }

    factory :recipe_with_ingredients do
      transient do
        ingredients_count {3}
      end
      after(:create) do |recipe, evaluator|
        create_list(:recipe_ingredient, evaluator.ingredients_count, recipe: recipe)
      end
    end
  end

  factory :invalid_recipe, class: "Recipe" do
    name {""}
    image {""}
    ingredients {""}
  end
end
