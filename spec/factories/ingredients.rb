# == Schema Information
#
# Table name: ingredients
#
#  id            :integer          not null, primary key
#  name          :string
#  energy        :float
#  fat           :float
#  proteins      :float
#  carbohydrates :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :ingredient do
    name          { Faker::Food.unique.ingredient }
    energy        { Faker::Number.decimal(2).to_f }
    fat           { Faker::Number.decimal(2).to_f }
    proteins      { Faker::Number.decimal(2).to_f }
    carbohydrates { Faker::Number.decimal(2).to_f }
  end

  factory :invalid_ingredient, class: 'Ingredient' do
    name          { "" }
    energy        { -1 }
    fat           { -1 }
    proteins      { -1 }
    carbohydrates { -1 }
  end
end
