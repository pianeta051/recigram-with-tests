# == Schema Information
#
# Table name: steps
#
#  id          :integer          not null, primary key
#  image       :string
#  description :string
#  seconds     :integer
#  order       :integer
#  recipe_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :step do
    image       { Faker::LoremPixel.image("1000x1000", false, 'food') }
    description { Faker::Lorem.sentence(30) }
    seconds     { Faker::Number.number(2).to_i }
    order       { 1 }
    recipe
  end

  factory :invalid_step, class: "Step" do
    image       { "" }
    description { "" }
    seconds     { -1 }
    order       { 0 }
    recipe      { nil }
  end
end
