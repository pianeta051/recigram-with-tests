require 'rails_helper'

RSpec.describe StepsController, type: :controller do

  before(:each) do
    @recipe = FactoryBot.create(:recipe)
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {
        recipe_id: @recipe.id
      }
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Step" do
        expect {
          post :create, params: {
            recipe_id: @recipe.id,
            step: FactoryBot.attributes_for(:step)
            }
        }.to change(Step, :count).by(1)
      end

      it "redirects to a new step form" do
        post :create, params: {
          recipe_id: @recipe.id,
          step: FactoryBot.attributes_for(:step)
        }
        expect(response).to redirect_to(new_recipe_step_path(@recipe))
      end

       it "redirects to the recipe when finished" do
         post :create, params: {
           finish: "Finish",
           recipe_id: @recipe.id,
           step: FactoryBot.attributes_for(:step)
         }
         expect(response).to redirect_to(recipe_path(@recipe))
       end

       it "assigns correct order" do
        FactoryBot.create_list(:step, 3, recipe: @recipe)
        post :create, params: {
          step: FactoryBot.attributes_for(:step),
          recipe_id: @recipe.id
        }
        expect(Step.last.order).to eq(4)
      end

    end

    context "with invalid params" do
      it "returns a success response" do
        post :create, params: {
          step: FactoryBot.attributes_for(:invalid_step),
            recipe_id: @recipe.id
          }
        expect(response).to be_successful
      end

      it "does not create a step" do
        expect{
          post :create, params: {
            step: FactoryBot.attributes_for(:invalid_step),
            recipe_id: @recipe.id
            }
        }.not_to change(Step, :count)
      end
    end
  end
  describe "PUT #update" do
    before(:each) do
      @step = FactoryBot.create(:step, recipe: @recipe)
      @new_step = FactoryBot.build(:step, recipe: @recipe)
    end

    context "with valid params" do
      it "updates the requested step" do
        put :update, params: {
          id: @step.to_param,
          step: @new_step.attributes,
          recipe_id: @recipe.id
        }
        @step.reload
        expect(@step).to have_attributes(@new_step.attributes.except("id", "created_at", "updated_at"))
      end

      it "redirects to the edit recipe page" do
        put :update, params: {
          id: @step.to_param,
          step: @new_step.attributes,
          recipe_id: @recipe.id
        }
        expect(response).to redirect_to(edit_recipe_path(@recipe))
      end
    end

    context "with invalid params" do
      it "does not update the step" do
        put :update, params: {
          id: @step.id,
          step: FactoryBot.attributes_for(:invalid_step),
          recipe_id: @recipe.id
        }
        expect{
          @step.reload
        }.not_to change{@step.attributes.except("id", "created_at", "updated_at")}
      end

      it "redirects to the edit recipe page" do
        put :update, params: {
          id: @step.id,
          step: FactoryBot.attributes_for(:invalid_step),
          recipe_id: @recipe.id
          }
        expect(response).to redirect_to(edit_recipe_path(@recipe))
      end
    end

  end

  describe "DELETE #destroy" do
    before(:each) do
      @step = FactoryBot.create(:step, recipe: @recipe)
    end

    context "when the step and the recipe exist" do

      it "destroys the requested step" do
        expect {
          delete :destroy, params: {
            id: @step.id,
            recipe_id: @recipe.id
          }
        }.to change(Step, :count).by(-1)
      end

      it "redirects to the recipe" do
        delete :destroy, params: {
          id: @step.id,
          recipe_id: @recipe.id
        }
        expect(response).to redirect_to(edit_recipe_path(@recipe))
      end
    end

    context "when the step does not exist" do
      it "does not delete the step" do
        expect {
          delete :destroy, params: {
            id: Step.count + 1,
            recipe_id: @recipe.id
          }
        }.not_to change(Step, :count)
      end

    end

    context "when the recipe does not exist" do
      it "does not delete the step" do
        expect {
          delete :destroy, params: {
            id: @step.id,
            recipe_id: Recipe.count + 1
          }
        }.not_to change(Step, :count)
      end
    end

  end

  describe "PUT #up" do
  before(:each) do
    @first_step = FactoryBot.create(:step, recipe: @recipe, order: 1)
    @second_step = FactoryBot.create(:step, recipe: @recipe, order: 2)
  end

  context "when the step is not the first" do
    it "updates the step order" do
      put :up, params: {
        recipe_id: @recipe.id,
        step_id: @second_step.id
      }
      @second_step.reload
      expect(@second_step.order).to eq(1)
    end

    it "updates the previous step order" do
      put :up, params: {
        recipe_id: @recipe.id,
        step_id: @second_step.id
      }
      @first_step.reload
      expect(@first_step.order).to eq(2)
    end

    it "redirects to the recipe edit page" do
      put :up, params: {
        recipe_id: @recipe.id,
        step_id: @second_step.id
      }
      expect(response).to redirect_to(edit_recipe_path(@recipe))
    end
  end

  context "when it's the first step" do
    it "does not update the step order" do
      put :up, params: {
        recipe_id: @recipe.id,
        step_id: @first_step.id
      }
      @first_step.reload
      expect(@first_step.order).to eq(1)
    end

    it "redirects to the recipe edit page" do
      put :up, params: {
        recipe_id: @recipe.id,
        step_id: @first_step.id
      }
      expect(response).to redirect_to(edit_recipe_path(@recipe))
    end
  end
end

describe "PUT #down" do
  before(:each) do
    @first_step = FactoryBot.create(:step, recipe: @recipe, order: 1)
    @last_step = FactoryBot.create(:step, recipe: @recipe, order: 2)
  end
  context "and is not the last" do
    it "updates the step order" do
      put :down, params: {
        recipe_id: @recipe.id,
        step_id: @first_step.id
      }
      @first_step.reload
      expect(@first_step.order).to eq(2)
    end

    it "updates the next step order" do
      put :down, params: {
        recipe_id: @recipe.id,
        step_id: @first_step.id
      }
      @last_step.reload
      expect(@last_step.order).to eq(1)
    end

    it "redirects to the recipe edit page" do
      put :down, params: {
        recipe_id: @recipe.id,
        step_id: @first_step.id
      }
      expect(response).to redirect_to(edit_recipe_path(@recipe))
    end
  end

  context "and it's the last step" do
    it "does not update the step order" do
      put :down, params: {
        recipe_id: @recipe.id,
        step_id: @last_step.id
      }
      @last_step.reload
      expect(@last_step.order).to eq(2)
    end

    it "redirects to the recipe edit page" do
      put :down, params: {
        recipe_id: @recipe.id,
        step_id: @last_step.id
      }
      expect(response).to redirect_to(edit_recipe_path(@recipe))
    end
  end
end


end
