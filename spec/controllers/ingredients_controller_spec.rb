require 'rails_helper'

RSpec.describe IngredientsController, type: :controller do

  let (:invalid_id) {
    Ingredient.count + 1
  }

  describe "GET #index" do
    it "returns a success response" do
      FactoryBot.create(:ingredient)
      get :index
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    context "when the ingredient exists" do
      it "returns a success response" do
        ingredient = FactoryBot.create(:ingredient)
        get :show, params: {id: ingredient.to_param}
        expect(response).to be_successful
      end
    end

    context "when the ingredient does not exist" do
      it "redirects to the ingredients list" do
        get :show, params: {id: :invalid_id}
        expect(response).to redirect_to(ingredients_path)
      end
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    context "when the ingredient exists" do
      it "returns a success response" do
        ingredient = FactoryBot.create(:ingredient)
        get :edit, params: {id: ingredient.to_param}
        expect(response).to be_successful
      end
    end

    context "when the ingredient does not exist" do
      it "redirects to the ingredients list" do
        get :edit, params: {id: :invalid_id}
        expect(response).to redirect_to(ingredients_path)
      end

    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Ingredient" do
        expect {
          post :create, params: {ingredient: FactoryBot.attributes_for(:ingredient)}
        }.to change(Ingredient, :count).by(1)
      end

      it "redirects to the created ingredient" do
        post :create, params: {ingredient: FactoryBot.attributes_for(:ingredient)}
        expect(response).to redirect_to(Ingredient.last)
      end
    end

    context "with invalid params" do
      it "returns a success response" do
        post :create, params: {ingredient: FactoryBot.attributes_for(:invalid_ingredient)}
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do

      it "updates the requested ingredient" do
        ingredient = FactoryBot.create(:ingredient)
        new_params = FactoryBot.attributes_for(:ingredient)
        put :update, params: {id: ingredient.to_param, ingredient: new_params}
        ingredient.reload
        expect(ingredient).to have_attributes(new_params.except("id", "created_at", "updated_at"))
      end

      it "redirects to the ingredient" do
        ingredient = FactoryBot.create(:ingredient)
        new_params = FactoryBot.attributes_for(:ingredient)
        put :update, params: {id: ingredient.to_param, ingredient: new_params}
        expect(response).to redirect_to(ingredient)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        ingredient = FactoryBot.create(:ingredient)
        put :update, params: {id: ingredient.to_param, ingredient: FactoryBot.attributes_for(:invalid_ingredient)}
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    context "when the ingredient exists" do
      before(:each) do
        @ingredient = FactoryBot.create(:ingredient)
      end

      it "destroys the requested ingredient" do
        expect {
          delete :destroy, params: {id: @ingredient.to_param}
        }.to change(Ingredient, :count).by(-1)
      end

      it "redirects to the ingredients list" do
        delete :destroy, params: {id: @ingredient.to_param}
        expect(response).to redirect_to(ingredients_url)
      end
    end

    context "when the ingredient does not exist" do
      it "does not destroy any ingredient" do
        expect {
          delete :destroy, params: {id: :invalid_id}
        }.not_to change(Ingredient, :count)
      end

      it "redirects to the ingredients list" do
        delete :destroy, params: {id: :invalid_id}
        expect(response).to redirect_to(ingredients_url)
      end
    end
  end

end
