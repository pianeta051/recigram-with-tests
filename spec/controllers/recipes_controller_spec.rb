require 'rails_helper'

RSpec.describe RecipesController, type: :controller do

  let (:invalid_id) {
    Recipe.count + 1
  }

  context 'without ingredients registered' do
    describe "GET #new" do
      it "redirects to ingredient creation" do
        get :new
        expect(response).to redirect_to new_ingredient_path
      end
    end
  end

  context 'with ingredients registered' do
    before(:each) do
      @ingredients_hash = {}
      4.times do
        FactoryBot.create(:ingredient)
      end
      Ingredient.all.each do |ingredient|
        @ingredients_hash[ingredient.id] = Faker::Number.decimal(2).to_f
      end
    end

    describe "GET #index" do
      it "returns a success response" do
        get :index
        expect(response).to be_successful
      end
    end

    describe "GET #new" do
      it "returns a success response" do
        get :new
        expect(response).to be_successful
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "redirects to new step" do
          post :create, params: {
            recipe: FactoryBot.attributes_for(:recipe),
            ingredients: @ingredients_hash
           }
          expect(response).to redirect_to(new_recipe_step_path(Recipe.last.id))
        end

        it "creates a new recipe" do
          expect{
            post :create, params: {
              recipe: FactoryBot.attributes_for(:recipe),
              ingredients: @ingredients_hash
            }
          }.to change(Recipe, :count).by(1)
        end

         it "adds the Ingredients to the recipe" do
           post :create, params: {
             recipe: FactoryBot.attributes_for(:recipe),
             ingredients: @ingredients_hash
           }
           recipe = Recipe.last
           @ingredients_hash.each do |id, quantity|
             if quantity > 0
               expect(recipe.ingredients.pluck(:id)).to include(id)
             else
               expect(recipe.ingredients.pluck(:id)).not_to include(id)
             end
           end
         end
      end

      context "with invalid params" do
        it "redirects to new recipe" do
          post :create, params: {
            recipe: FactoryBot.attributes_for(:invalid_recipe),
            ingredients: {}
           }
          expect(response).to be_successful
        end

        it "don't create a new recipe" do
          expect{
            post :create, params: { recipe: FactoryBot.attributes_for(:invalid_recipe)}
          }.to_not change(Recipe, :count)
        end
      end

    end

    describe "GET #show" do
      context "when the recipe exists" do
        it "returns a success response" do
          recipe = FactoryBot.create(:recipe)
          get :show, params: {id: recipe.id}
          expect(response).to be_successful
        end
      end

      context "when the recipe does not exist" do
        it "redirects to recipes list" do
          get :show, params: {id: :invalid_id}
          expect(response).to redirect_to(recipes_path)
        end
      end
    end

    describe "GET #edit" do
      context "when the recipe exists" do
        it "returns a success response" do
          recipe = FactoryBot.create(:recipe)
          get :edit, params: {id: recipe.id}
          expect(response).to be_successful
        end
      end

      context "when the recipes does not exist" do
        it "redirects to recipes list" do
          get :edit, params: {id: :invalid_id}
          expect(response).to redirect_to(recipes_path)
        end
      end
    end

    describe "PUT #update" do
      context "when the recipe exists" do
        before(:each) do
          @recipe = FactoryBot.create(:recipe_with_ingredients, ingredients_count: 2)
          @ingredients_hash[@recipe.ingredients.first.id] = 0
          @ingredients_hash[@recipe.ingredients.last.id] = 3
        end

        context "and attributes are correct" do
          before(:each) do
            @new_params = FactoryBot.attributes_for(:recipe)
            put :update, params: {
              id: @recipe.id,
              recipe: @new_params,
              ingredients: @ingredients_hash }
          end

          it "redirects to the recipes list" do
            expect(response).to redirect_to(recipes_path)
          end

          it "changes the recipe attributes" do
            @recipe.reload
            expect(@recipe).to have_attributes(@new_params.except("id"))
          end

          it "updates its ingredients" do
            @recipe.reload
            @ingredients_hash.each do |id, quantity|
              if quantity > 0
                expect(@recipe.ingredients.pluck(:id)).to include(id)
              else
                expect(@recipe.ingredients.pluck(:id)).not_to include(id)
              end
            end
          end
        end

        context "and attributes are wrong" do
          it "stays in the same page" do
            expect(response).to be_successful
          end

          it "does not change the recipe" do
            expect{
              put :update, params: {
                id: @recipe.id,
                recipe: FactoryBot.attributes_for(:invalid_recipe),
                ingredients: @ingredients_hash }
              @recipe.reload
            }.not_to change {@recipe}
          end
        end
      end

      context "when the recipe does not exist" do
        it "redirects to recipes list" do
          put :update, params: {id: :invalid_id, recipe: FactoryBot.attributes_for(:recipe)}
          expect(response).to redirect_to(recipes_path)
        end
      end
    end

    describe "DELETE #destroy" do
      context "when the recipe exists" do
        before(:each) do
          @recipe = FactoryBot.create(:recipe)
        end

        it "redirects to the recipes list" do
          delete :destroy, params: { id: @recipe.id }
          expect(response).to redirect_to(recipes_path)
        end

        it "destroys the seleted recipe" do
          expect{
            delete :destroy, params: { id: @recipe.id }
          }.to change(Recipe, :count).by(-1)
        end
      end

      context "when the recipe does not exist" do
        it "does not destroy any recipe" do
          expect{
            delete :destroy, params: { id: :invalid_id }
          }.not_to change(Recipe, :count)
        end
      end
    end
  end


end
