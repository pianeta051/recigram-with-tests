# == Schema Information
#
# Table name: recipes
#
#  id    :integer          not null, primary key
#  name  :string
#  image :string
#

require 'rails_helper'

RSpec.describe Recipe, type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:image).of_type(:string) }
  it { is_expected.not_to have_db_column(:ingredients).of_type(:string) }
  it { is_expected.not_to have_db_column(:steps).of_type(:string) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:image) }
  it { is_expected.to validate_length_of(:name).is_at_most(300) }
  it { is_expected.to have_many(:recipe_ingredients) }
  it { is_expected.to have_many(:ingredients).through(:recipe_ingredients) }

  it 'returns the ingredient quantity' do
    recipe = FactoryBot.create(:recipe_with_ingredients, ingredients_count: 1)
    ingredient = recipe.ingredients.first
    quantity = recipe.recipe_ingredients.first.quantity
    expect(recipe.quantity(ingredient)).to eq(quantity)
    ingredientZero = FactoryBot.create(:ingredient)
    expect(recipe.quantity(ingredientZero)).to eq(0)
  end

  context 'calculating nutritional info' do

    before(:all) do
      @recipe = FactoryBot.create(:recipe_with_ingredients, ingredients_count: 2)
      @recipe.recipe_ingredients.first.update(quantity: 100)
      @recipe.recipe_ingredients.last.update(quantity: 50)
    end

    it 'calculates its energy' do
      energy = @recipe.ingredients.first.energy +  @recipe.ingredients.last.energy/2
      expect(@recipe.energy).to be_within(0.1).of(energy)
    end

    it 'calculates its proteins' do
      proteins = @recipe.ingredients.first.proteins +  @recipe.ingredients.last.proteins/2
      expect(@recipe.proteins).to be_within(0.1).of(proteins)
    end

    it 'calculates its carbohydrates' do
      carbohydrates = @recipe.ingredients.first.carbohydrates +  @recipe.ingredients.last.carbohydrates/2
      expect(@recipe.carbohydrates).to be_within(0.1).of(carbohydrates)
    end

    it 'calculates its fat' do
      fat = @recipe.ingredients.first.fat +  @recipe.ingredients.last.fat/2
      expect(@recipe.fat).to be_within(0.1).of(fat)
    end
  end

  context 'calculating cook time' do
    it 'calculates sum of times' do
      recipe = FactoryBot.create(:recipe)
      FactoryBot.create(:step, recipe: recipe, order: 1, seconds: 50)
      FactoryBot.create(:step, recipe: recipe, order: 2, seconds: 150)
      FactoryBot.create(:step, recipe: recipe, order: 3, seconds: 50)

      expect(recipe.cook_time).to eq(250)
    end

    it 'returns zero with no steps' do
      recipe = FactoryBot.create(:recipe)
      expect(recipe.cook_time).to eq(0)
    end
  end


end
