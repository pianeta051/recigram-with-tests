# == Schema Information
#
# Table name: steps
#
#  id          :integer          not null, primary key
#  image       :string
#  description :string
#  seconds     :integer
#  order       :integer
#  recipe_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Step, type: :model do
  it { is_expected.to have_db_column(:image).of_type(:string) }
  it { is_expected.to have_db_column(:description).of_type(:string) }
  it { is_expected.to have_db_column(:order).of_type(:integer) }
  it { is_expected.to have_db_column(:seconds).of_type(:integer) }
  it { is_expected.to validate_presence_of(:image) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:seconds) }
  it { is_expected.to validate_presence_of(:order) }
  it { is_expected.to validate_length_of(:image).is_at_most(500) }
  it { is_expected.to validate_length_of(:description).is_at_most(500) }
  it { is_expected.to validate_numericality_of(:seconds).is_greater_than_or_equal_to(0).with_message("can't be negative") }
  it { is_expected.to validate_numericality_of(:order).is_greater_than(0).with_message("can't be negative") }

end
