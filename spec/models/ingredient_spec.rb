# == Schema Information
#
# Table name: ingredients
#
#  id            :integer          not null, primary key
#  name          :string
#  energy        :float
#  fat           :float
#  proteins      :float
#  carbohydrates :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:energy).of_type(:float) }
  it { is_expected.to have_db_column(:fat).of_type(:float) }
  it { is_expected.to have_db_column(:proteins).of_type(:float) }
  it { is_expected.to have_db_column(:carbohydrates).of_type(:float) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_numericality_of(:energy).is_greater_than_or_equal_to(0).with_message("Energy can't be negative") }
  it { is_expected.to validate_length_of(:name).is_at_most(200) }
  it { is_expected.to have_many(:recipe_ingredients) }
  it { is_expected.to have_many(:recipes).through(:recipe_ingredients) }
end
