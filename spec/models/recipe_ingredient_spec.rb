# == Schema Information
#
# Table name: recipe_ingredients
#
#  id            :integer          not null, primary key
#  recipe_id     :integer
#  ingredient_id :integer
#  quantity      :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe RecipeIngredient, type: :model do
  it { is_expected.to have_db_column(:recipe_id).of_type(:integer) }
  it { is_expected.to have_db_column(:ingredient_id).of_type(:integer) }
  it { is_expected.to have_db_column(:quantity).of_type(:float) }
  it { is_expected.to belong_to(:recipe) }
  it { is_expected.to belong_to(:ingredient) }
  it { is_expected.to validate_numericality_of(:quantity).is_greater_than_or_equal_to(0).with_message("can't be negative") }
end
