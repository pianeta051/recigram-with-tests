require 'rails_helper'

RSpec.feature "Reading an ingredient", :type => :feature do
  scenario "when the ingredient doesn't exist" do
    last_ingredient_id = Ingredient.count
    visit "ingredients/#{last_ingredient_id+1}"
    expect(page).to have_content("This ingredient does not exist")
  end
end
