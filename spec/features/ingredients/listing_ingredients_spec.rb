
require 'rails_helper'

RSpec.feature "Indexing ingredients", :type => :feature do
  scenario 'when there are no ingredients' do
    visit ingredients_path
    expect(page).to have_content("There are no ingredients yet")
  end
end
