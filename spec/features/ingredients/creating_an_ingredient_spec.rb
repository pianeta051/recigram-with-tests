require 'rails_helper'

RSpec.feature "Creating new ingredient", :type => :feature do

  before (:each) do
    @ingredient = FactoryBot.build(:ingredient)
  end

  scenario 'with blank name' do
    @ingredient.name = ""
    submit_ingredient(@ingredient)
    expect(page).to have_content("Name can't be blank")
  end

  scenario 'with negative energy' do
    @ingredient.energy = -1
    submit_ingredient(@ingredient)
    expect(page).to have_content("Energy can't be negative")
  end

  scenario 'with a very long name' do
    @ingredient.name = "a" * 201
    submit_ingredient(@ingredient)
    expect(page).to have_content("Name is too long")
  end

  def submit_ingredient(ingredient)
    visit new_ingredient_path
    fill_in "ingredient_name", with: ingredient.name
    fill_in "ingredient_energy", with: ingredient.energy
    fill_in "ingredient_fat", with: ingredient.fat
    fill_in "ingredient_proteins", with: ingredient.proteins
    fill_in "ingredient_carbohydrates", with: ingredient.carbohydrates
    click_button "Save"
  end

end
