require 'rails_helper'

RSpec.feature "Editing an ingredient", :type => :feature do
  scenario "when the ingredient doesn't exist" do
    last_ingredient_id = Ingredient.count
    visit "ingredients/#{last_ingredient_id+1}/edit"
    expect(page).to have_content("This ingredient does not exist")
  end

  scenario "with blank name" do
    ingredient = FactoryBot.create(:ingredient)
    visit edit_ingredient_path(ingredient)
    fill_in "ingredient_name", with: ""
    click_on "Save"
    expect(page).to have_content("Name can't be blank")
  end
end
