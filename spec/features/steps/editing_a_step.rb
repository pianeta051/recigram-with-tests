require 'rails_helper'

RSpec.feature "Editing a step", :type => :feature do

  before (:each) do
    @recipe = FactoryBot.create(:recipe)
    @step = FactoryBot.create(:step, recipe: @recipe)
  end

  scenario 'with blank image' do
    @step.image = ""
    edit_step(@step)
    expect(page).to have_content("Image can't be blank")
  end

  # scenario 'with blank description' do
  #   @step.description = ""
  #   edit_step(@step)
  #   check_error_message("Description", "blank")
  # end
  #
  # scenario 'with blank seconds' do
  #   @step.seconds = ""
  #   edit_step(@step)
  #   check_error_message("Seconds", "blank")
  # end
  #
  # scenario 'with a very long image' do
  #   @step.image = "a" * 501
  #   edit_step(@step)
  #   check_error_message("Image", "too long")
  # end
  #
  # scenario 'with a very long description' do
  #   @step.description = "a" * 501
  #   edit_step(@step)
  #   check_error_message("Description", "too long")
  # end
  #
  # scenario 'with negative seconds' do
  #   @step.seconds = -1
  #   edit_step(@step)
  #   check_error_message("Seconds", "negative")
  # end

  def edit_step(step)
    visit edit_recipe_path(@recipe)
    fill_in "step_#{step.order}_image", with: step.image
    fill_in "step_#{step.order}_description", with: step.description
    fill_in "step_#{step.order}_seconds", with: step.seconds
    page.find('div.list-group-item', text: step.order).click_on "Save"
  end

end
