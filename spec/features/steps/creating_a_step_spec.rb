
require 'rails_helper'

RSpec.feature "Creating new step", :type => :feature do

  before (:each) do
    @recipe = FactoryBot.create(:recipe)
    @step = FactoryBot.build(:step, recipe: @recipe)
  end

  scenario 'with blank image' do
    @step.image = ""
    submit_step(@step, true)
    expect(page).to have_content("Image can't be blank")
  end

  scenario 'with blank description' do
    @step.description = ""
    submit_step(@step)
    expect(page).to have_content("Description can't be blank")
  end

  scenario 'with blank seconds' do
    @step.seconds = ""
    submit_step(@step)
    expect(page).to have_content("Seconds can't be blank")
  end

  scenario 'with a very long image' do
    @step.image = "a" * 501
    submit_step(@step)
    expect(page).to have_content("Image is too long")
  end

  scenario 'with a very long description' do
    @step.description = "a" * 501
    submit_step(@step)
    expect(page).to have_content("Description is too long")
  end

  scenario 'with negative seconds' do
    @step.seconds = -1
    submit_step(@step)
    expect(page).to have_content("Seconds can't be negative")
  end

  def check_error_message (attribute, type)
    expect(page).to have_content(error_message(attribute, type))
  end

  def submit_step(step, edit = false)
    if edit
      visit edit_recipe_path(@recipe)
      click_on "New step"
    else
      visit new_recipe_step_path(@recipe)
    end
    fill_in "step_image", with: step.image
    fill_in "step_description", with: step.description
    fill_in "step_seconds", with: step.seconds
    click_on "Finish"
  end

end
