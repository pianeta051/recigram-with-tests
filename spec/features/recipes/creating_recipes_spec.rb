require 'rails_helper'

RSpec.feature "Creating recipes", type: :feature do
  before(:each) do
    @recipe = FactoryBot.build(:recipe)
    FactoryBot.create_list(:ingredient, 10)
    @ingredient1 = Ingredient.first
    @ingredient2 = Ingredient.last
  end

  scenario 'with blank name' do
    @recipe.name = ""
    submit_recipe(@recipe)
    expect(page).to have_content("Name can't be blank")
  end

  scenario 'with blank image' do
    @recipe.image = ""
    submit_recipe(@recipe)
    expect(page).to have_content("Image can't be blank")
  end

  scenario 'with name too long' do
    @recipe.name = "a"*301
    submit_recipe(@recipe)
    expect(page).to have_content("Name is too long")
  end

  scenario 'with negative quantity' do
    submit_recipe(@recipe, -1)
    expect(page).to have_content("quantity can't be negative")
  end

  scenario 'with no ingredients' do
    submit_recipe(@recipe, 0, 0)
    expect(page).to have_content("Ingredients can't be blank")
  end

  def submit_recipe(recipe, quantity1 = 100, quantity2 = 100)
    visit new_recipe_path
    fill_in "recipe_name", with: recipe.name
    fill_in "recipe_image", with: recipe.image
    fill_in "#{@ingredient1.name}_quantity", with: quantity1
    fill_in "#{@ingredient2.name}_quantity", with: quantity2
    click_button "Create steps"
  end
end
