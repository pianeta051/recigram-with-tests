require 'rails_helper'

RSpec.feature "Showing a recipe", :type => :feature do
  scenario 'when the recipe does not exist' do
    last_recipe_id = Recipe.count
    visit "recipes/#{last_recipe_id+1}"
    expect(page).to have_content("This recipe does not exist")
  end

  scenario 'when it has no steps' do
    recipe = FactoryBot.create(:recipe)
    visit recipe_path(recipe)
    expect(page).to have_content("This recipe has no steps")
  end

end
