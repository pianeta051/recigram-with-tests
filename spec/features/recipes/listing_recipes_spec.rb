require 'rails_helper'

RSpec.feature "Listing recipes", type: :feature do
  scenario 'when there are no recipes' do
    visit recipes_path
    expect(page).to have_content("There are no recipes yet")
  end
end
