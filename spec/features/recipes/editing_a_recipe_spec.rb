require 'rails_helper'

RSpec.feature "Editing a recipe", :type => :feature do

  context "editing steps form" do
    scenario 'when the recipe does not exist' do
      last_recipe_id = Recipe.count
      visit "recipes/#{last_recipe_id+1}/edit"
      expect(page).to have_content("This recipe does not exist")
    end

    before(:each) do
      @recipe = FactoryBot.create(:recipe_with_ingredients)
      @new_params = FactoryBot.build(:recipe)
      FactoryBot.create_list(:ingredient, 10)
      @ingredient1 = Ingredient.first
      @ingredient2 = Ingredient.last
    end

    scenario 'with blank name' do
      @new_params.name = ""
      edit_recipe(@new_params)
      expect(page).to have_content("Name can't be blank")
    end

    scenario 'edit form should have ingredients quantities' do
      visit edit_recipe_path(@recipe)
      @recipe.recipe_ingredients.each do |ri|
        expect(find_field("#{ri.ingredient.name}_quantity").value.to_f).to eq(ri.quantity)
      end
    end

    def edit_recipe(recipe, quantity1 = 100, quantity2 = 100)
      visit edit_recipe_path(@recipe)
      fill_in "recipe_name", with: recipe.name
      fill_in "recipe_image", with: recipe.image
      fill_in "#{@ingredient1.name}_quantity", with: quantity1
      fill_in "#{@ingredient2.name}_quantity", with: quantity2
      click_button "Save"
    end
  end

  context "checking buttons" do
    before(:each) do
      @recipe = FactoryBot.create(:recipe)
      5.times do |i|
        FactoryBot.create(:step, recipe: @recipe, order: i+1)
      end
      @first_step = @recipe.steps.order(:order).first
      @last_step = @recipe.steps.order(:order).last
    end

    scenario "first step does not have up button" do
      visit edit_recipe_path(@recipe)
      expect(page.find('div.list-group-item', text: @first_step.description)).not_to have_content('Up')
    end

    scenario "last step does not have down button" do
      visit edit_recipe_path(@recipe)
      expect(page.find('div.list-group-item', text: @last_step.description)).not_to have_content('Down')
    end
  end

end
